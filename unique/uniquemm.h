#ifndef _UNIQUEMM_H
#define _UNIQUEMM_H

/* uniquemm.h
 * 
 * Copyright (C) 2009  Fabien Parent <parent.f@gmail.com>
 * 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include <uniquemm/app.h>
#include <uniquemm/message.h>
#include <uniquemm/backend.h>
#include <uniquemm/version.h>

#endif /* #ifndef _UNIQUEMM_H */

