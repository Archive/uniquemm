/* Copyright (c) 2009  Fabien Parent <parent.f@gmail.com>
 *
 * This file is part of libuniquemm.
 *
 * libuniquemm is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * libuniquemm is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UNIQUEMM_WRAP_INIT_H_INCLUDED
#define UNIQUEMM_WRAP_INIT_H_INCLUDED

namespace Unique { void wrap_init(); }

#endif /* !UNIQUEMM_WRAP_INIT_H_INCLUDED */
