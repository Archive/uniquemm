/** 
 * Copyright (C) 2009  Fabien Parent <parent.f@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gtkmm.h>
#include <uniquemm.h>

#include <iostream>
#include <vector>

class MyApp : public Unique::App
{
public:
	enum
	{
		COMMAND_0,
		
		COMMAND_FOO,
		COMMAND_BAR
	};
	
	MyApp(void) :
		Unique::App("org.gnome.TestUnique", "test"),
		window_(),
		new_option_(false),
		activate_option_(false),
		uris_(),
		foo_option_(false)
	{
		add_command("foo", MyApp::COMMAND_FOO);
		add_command("bar", MyApp::COMMAND_BAR);

		signal_message_received().connect(sigc::mem_fun(*this, &MyApp::on_message_received), false);
	}
	
	bool parse_cmd_line(int& argc, char**& argv)
	{
		Glib::OptionContext context;
		Glib::OptionGroup group("Test libuniquemm", "");
		
		Glib::OptionEntry entry_new;
		entry_new.set_long_name("new");
		entry_new.set_short_name('n');
		entry_new.set_description("Send 'new' command");
		
		Glib::OptionEntry entry_open_uri;
		entry_open_uri.set_long_name("open-uri");
		entry_open_uri.set_short_name('o');
		entry_open_uri.set_description("Send 'open' command");
		entry_open_uri.set_arg_description("URI");
		entry_open_uri.set_flags(Glib::OptionEntry::FLAG_FILENAME);
		
		Glib::OptionEntry entry_activate;
		entry_activate.set_long_name("activate");
		entry_activate.set_short_name('a');
		entry_activate.set_description("Send 'activate' command");
		
		Glib::OptionEntry entry_foo;
		entry_foo.set_long_name("foo");
		entry_foo.set_short_name('f');
		entry_foo.set_description("Send 'foo' command");
		
		group.add_entry(entry_new, new_option_);
		group.add_entry(entry_activate, activate_option_);
		group.add_entry(entry_open_uri, uris_);
		group.add_entry(entry_foo, foo_option_);
		
		context.add_group(group);
		context.set_ignore_unknown_options(true);
		
		try
		{
			context.parse(argc, argv);
		}
		catch (...)
		{
			std::cout << context.get_help() << std::endl;
			return false;
		}
		
		return true;
	}
	
	void run(void)
	{
		if (is_running())
		{
			Unique::MessageData message;
			Unique::Response response;
			int command;
			
			if (new_option_)
				command = Unique::NEW;
			else if (!uris_.empty())
			{
				command = Unique::OPEN;
				message.set_uris(uris_);
			}
			else if (activate_option_)
				command = Unique::ACTIVATE;
			else if (foo_option_)
			{
				command = COMMAND_FOO;
				message.set_text("bar");
			}
			else
				command = COMMAND_BAR;
			response = send_message(command, message);
			
			std::cout << "Response code: " << response << std::endl;
			
			gdk_notify_startup_complete ();
		}
		else
		{
			watch_window(window_);
			Gtk::Main::run(window_);
		}
	}

protected:
	Unique::Response on_message_received(int command, Unique::MessageData message_data, guint time)
	{
		Glib::ustring title;
		Glib::ustring message;
		
		std::cout << "Message received from screen: " << message_data.get_screen()->get_number()
				  << ", startup-id: " << message_data.get_startup_id()
				  << ", workspace: " << message_data.get_workspace()
				  << std::endl;
		
		switch (command)
		{
			case Unique::NEW:
				title = "Received the NEW command";
			break;
		
			case Unique::ACTIVATE:
				title = "Received the ACTIVATE command";
			break;
		
			case Unique::OPEN:
				title = "Received the OPEN command";
				
				uris_ = message_data.get_uris();
				for (std::vector<Glib::ustring>::iterator uri = uris_.begin(); uri != uris_.end(); uri++)
					message += *uri + '\n';
			break;
		
			case MyApp::COMMAND_FOO:
				title = "Received the FOO command";
				message = message_data.get_text();
			break;
		
			case MyApp::COMMAND_BAR:
				title = "Received the BAR command";
				message = "Thid command doesn't do anything special";
			break;
		}
		
		Gtk::MessageDialog dialog(window_, title);
		dialog.set_secondary_text(message);
		dialog.set_urgency_hint(true);
		dialog.run();
	
		return Unique::RESPONSE_OK;
	}

private:
	Gtk::Window window_;
	bool new_option_;
	bool activate_option_;
	std::vector<Glib::ustring> uris_;
	bool foo_option_;
};

int main(int argc, char** argv)
{
	Gtk::Main kit(argc, argv);
	MyApp app;
	
	if (app.parse_cmd_line(argc, argv))
		app.run();
	
	return 0;
}
