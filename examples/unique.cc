/** 
 * Copyright (C) 2009  Fabien Parent <parent.f@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gtkmm.h>
#include <uniquemm.h>

#include <iostream>
#include <vector>

Gtk::Window* window;

enum
{
	COMMAND_0,
	
	COMMAND_FOO,
	COMMAND_BAR
};

Unique::Response on_message_received(int command, Unique::MessageData message_data, unsigned int time)
{
	Glib::ustring title;
	Glib::ustring message;
	std::vector<Glib::ustring> uris;
	
	std::cout << "Message received from screen: " << message_data.get_screen()->get_number()
			  << ", startup-id: " << message_data.get_startup_id()
			  << ", workspace: " << message_data.get_workspace()
			  << std::endl;
	
	switch (command)
	{
		case Unique::NEW:
			title = "Received the NEW command";
		break;
		
		case Unique::ACTIVATE:
			title = "Received the ACTIVATE command";
		break;
		
		case Unique::OPEN:
			title = "Received the OPEN command";
			
			uris = message_data.get_uris();
			for (std::vector<Glib::ustring>::iterator uri = uris.begin(); uri != uris.end(); uri++)
				message += *uri + '\n';
		break;
		
		case COMMAND_FOO:
			title = "Received the FOO command";
			message = message_data.get_text();
		break;
		
		case COMMAND_BAR:
			title = "Received the BAR command";
			message = "Thid command doesn't do anything special";
		break;
	}
	Gtk::MessageDialog dialog(*window, title);
	dialog.set_secondary_text(message);
	dialog.set_urgency_hint(true);
	dialog.run();
	
	return Unique::RESPONSE_OK;
}

int main(int argc, char** argv)
{
	Glib::init();
	Glib::OptionContext context;
	Glib::OptionGroup group("Test libuniquemm", "");
	bool new_option = false;
	bool activate_option = false;
	std::vector<Glib::ustring> uris;
	bool foo_option = false;
	
	Glib::OptionEntry entry_new;
	entry_new.set_long_name("new");
	entry_new.set_short_name('n');
	entry_new.set_description("Send 'new' command");
	
	Glib::OptionEntry entry_open_uri;
	entry_open_uri.set_long_name("open-uri");
	entry_open_uri.set_short_name('o');
	entry_open_uri.set_description("Send 'open' command");
	entry_open_uri.set_arg_description("URI");
	entry_open_uri.set_flags(Glib::OptionEntry::FLAG_FILENAME);
	
	Glib::OptionEntry entry_activate;
	entry_activate.set_long_name("activate");
	entry_activate.set_short_name('a');
	entry_activate.set_description("Send 'activate' command");
	
	Glib::OptionEntry entry_foo;
	entry_foo.set_long_name("foo");
	entry_foo.set_short_name('f');
	entry_foo.set_description("Send 'foo' command");
	
	group.add_entry(entry_new, new_option);
	group.add_entry(entry_activate, activate_option);
	group.add_entry(entry_open_uri, uris);
	group.add_entry(entry_foo, foo_option);
	
	context.add_group(group);
	context.set_ignore_unknown_options(true);
	
	try
	{
		context.parse(argc, argv);
	}
	catch (...)
	{
		std::cout << context.get_help() << std::endl;
		return 1;
	}
	
	Gtk::Main kit(argc, argv);
	window = new Gtk::Window;
	Glib::RefPtr<Unique::App> app = Unique::App::create("org.gnome.TestUnique", "test");
	app->add_command("foo", COMMAND_FOO);
	app->add_command("bar", COMMAND_BAR);
	
	if (app->is_running())
	{
		Unique::MessageData message;
		Unique::Response response;
		int command;
		
		if (new_option)
			command = Unique::NEW;
		else if (!uris.empty())
		{
			command = Unique::OPEN;
			message.set_uris(uris);
		}
		else if (activate_option)
			command = Unique::ACTIVATE;
		else if (foo_option)
		{
			command = COMMAND_FOO;
			message.set_text("bar");
		}
		else
			command = COMMAND_BAR;
		response = app->send_message(command, message);

		std::cout << "Response code: " << response << std::endl;
	}
	else
	{
		app->watch_window(*window);
		app->signal_message_received().connect(sigc::ptr_fun(&on_message_received), false);
		Gtk::Main::run(*window);
	}
	
	return 0;
}
