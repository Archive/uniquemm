/** 
 * Copyright (C) 2009  Fabien Parent <parent.f@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gtkmm.h>
#include <uniquemm.h>

#include <iostream>

enum
{
	COMMAND_0,
	
	COMMAND_FOO
};

Unique::Response on_message_received(int command, Unique::MessageData message_data, unsigned int time)
{
	std::cout << "Message received from screen: " << message_data.get_screen()->get_number()
			  << ", startup-id: " << message_data.get_startup_id()
			  << ", workspace: " << message_data.get_workspace()
			  << ", text: " << message_data.get_text()
			  << std::endl;
	
	return Unique::RESPONSE_OK;
}

int main(int argc, char** argv)
{
	Gtk::Main kit(argc, argv);
	Glib::RefPtr<Unique::App> app = Unique::App::create("org.gnome.TestUnique", "test");
	app->add_command("foo", COMMAND_FOO);
	
	if (app->is_running())
	{
		Unique::MessageData message;
		Unique::Response response;
		int command = COMMAND_FOO;
		message.set_text("bar");
		response = app->send_message(command, message);

		std::cout << "Response code: " << response << std::endl;
	}
	else
	{
		Gtk::Window window;
		app->watch_window(window);
		app->signal_message_received().connect(sigc::ptr_fun(&on_message_received), false);
		Gtk::Main::run(window);
	}
	
	return 0;
}
