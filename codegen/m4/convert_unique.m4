dnl Copyright (c) 2009  Fabien Parent <parent.f@gmail.com>
dnl This file is part of libuniquemm.

_CONVERSION(`Gtk::Window&',`GtkWindow*',$3.gobj())
_CONVERSION(`GtkWindow*',`Gtk::Window&',Glib::wrap($3))

_CONVERSION(`UniqueMessageData*', `MessageData', MessageData($3))
_CONVERSION(`UniqueMessageData*', `MessageData*', &(MessageData($3)))
_CONVERSION(`MessageData', `UniqueMessageData*', $3.gobj())
_CONVERSION(`MessageData&', `UniqueMessageData*', $3.gobj())
_CONVERSION(`MessageData*', `UniqueMessageData*', $3->gobj())

_CONVERSION(`const Glib::StringArrayHandle&', `gchar**', const_cast<gchar**>($3.data()))
_CONVERSION(`gchar**', `Glib::StringArrayHandle', Glib::StringArrayHandle($3, Glib::OWNERSHIP_DEEP))

_CONVERSION(`GdkScreen', `Glib::RefPtr<Gdk::Screen>', Glib::wrap($3, true))

_CONV_ENUM(Unique, Response)
_CONV_ENUM(Unique, Command)
